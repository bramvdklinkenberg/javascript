Algorithm - Modification for two bits

The modification for 2 bits was quite simple. Pixel values in original image 1 (the image to hide another image) are determined by floor(pixelvalue/2^2) * 2^2. This expression sets the 2^0 and 2^1 bit to 0 in binary. In order to take the first two bits of the hidden image for 2^6 and 2^7 and place them in the 2^0 and 2^1 bits, the pixel values are calculated with floor(pixelvalue/2^6). The remaining steps of the algorithm are similar to the 4-bit approach. In order to extract the hidden image, only the first two bits in the 2^0 and 2^1 entries are taken from the combined image and placed in the 2^6 and 2^7 entries for each pixel. This produces the hidden image with poor resolution since pixels are described using only two bits.
Code Modification for 2 Bits

// Part 3: Hiding Image in first 2 bits

function shift(im){
    var nim = new SimpleImage(im.getWidth(),im.getHeight()); 
    for(var px of im.values()){ 
        var x = px.getX();
        var y = px.getY();
        var npx = nim.getPixel(x,y);
        npx.setRed(Math.floor(px.getRed()/64));
        npx.setGreen(Math.floor(px.getGreen()/64));
        npx.setBlue(Math.floor(px.getBlue()/64));
    }
    return nim;
}

function pixchange(pixval) {
    var x = Math.floor(pixval/4) * 4;
    return x;
}

function chop2hide(image){
    for(var px of image.values()){
        px.setRed(pixchange(px.getRed()));
        px.setGreen(pixchange(px.getGreen()));
        px.setBlue(pixchange(px.getBlue()));
    }
    return image;
}

function crop(image, width, height) {
    var nimage = new SimpleImage(width,height)
    for (var px of nimage.values()) {
        var xval = px.getX()
        var yval = px.getY()
        var npx = image.getPixel(xval, yval)
        nimage.setPixel(xval, yval, npx)
    }
    return nimage
}

function newpv(p, q) {
    return p + q
}

function combine(image1, image2) {
    var width = image1.getWidth();
    var height = image1.getHeight()
    var nimage = new SimpleImage(width,height)
    
    for (var px of image1.values()) {
        var xval = px.getX()
        var yval = px.getY()
        var npx1 = image1.getPixel(xval, yval)
        var npx2 = image2.getPixel(xval, yval)
        
        var nnpx = npx1;
        
        nnpx.setRed(npx1.getRed() + npx2.getRed());
        nnpx.setGreen(npx1.getGreen() + npx2.getGreen());
        nnpx.setBlue(npx1.getBlue() + npx2.getBlue());
        
        nimage.setPixel(xval, yval, nnpx)
    }
    return(nimage)
}


function extract(image) {
    var width = image.getWidth();
    var height = image.getHeight()
    var nimage = new SimpleImage(width,height)
    for (var px of image.values()) {
        var xval = px.getX()
        var yval = px.getY()
        
        px.setRed(convBin(px.getRed()))
        px.setGreen(convBin(px.getGreen()))
        px.setBlue(convBin(px.getBlue()))
        
        nimage.setPixel(xval, yval, px)
        
    }
	return(nimage)
}

function convBin(n) {
    var n128 = Math.floor(n/128)
    var n64 = Math.floor((n - n128*128) /64)
    var n32 = Math.floor((n - n128*128 - n64*64)/32)
    var n16 = Math.floor((n - n128*128 - n64*64 - n32*32)/16)
    var n8 = Math.floor((n - n128*128 - n64*64 - n32*32 - n16*16)/8)
    var n4 = Math.floor((n - n128*128 - n64*64 - n32*32 - n16*16 - n8*8)/4)
    var n2 = Math.floor((n - n128*128 - n64*64 - n32*32 - n16*16 - n8*8 - n4*4)/2)
    var n1 = Math.floor((n - n128*128 - n64*64 - n32*32 - n16*16 - n8*8 - n4*4 - n2*2)/1)
    var newval = n1*64 + n2*128
    return newval
}


// Load images
var start = new SimpleImage("pixabayhands.jpg")
var hide = new SimpleImage("palm-and-beach.png")

// crop picture
var start = crop(start, hide.getWidth(), hide.getHeight())
print(start)  // cropped picture

// chop last bits
var comb_start = chop2hide(start);
// shift image to be hidden
var shift_hide = shift(hide);

// Combine images
var final = combine(comb_start, shift_hide);
print(final)  // combine image

// Extract hidden image
var ext = extract(final)
print(ext) // extracted image